<!--
SPDX-FileCopyrightText: 2024 John Colagioia <jcolag@colagioia.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta
    http-equiv="Cache-Control"
    content="no-cache, no-store, must-revalidate"
    />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="Thu, 1 Jan 1970 00:00:00 GMT" />
  <link rel="stylesheet" href="styles/default.css" />
  <script>
    var _paq = window._paq = window._paq || [];
    _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
    _paq.push(["setCookieDomain", "*.thelightsedge.com"]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://colagioia.net/pa/";
      _paq.push(['setTrackerUrl', u+'matomo.php']);
      _paq.push(['setSiteId', '17']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <title>Contributing to The Light's Edge</title>
</head>
<body>
  <h1>Contributing</h1>
  <p>
    First, let&rsquo;s thank you for even considering clicking over to this
    page.  Even if you only tell other people about the project, you have
    our thanks.
  </p>
  <h2>Financial Contributions</h2>
  <p>
    No time to spare but some cash burning a hole in your pocket?  We&rsquo;ll
    use it to keep the lights on, here, and commission more work for the
    project.
  </p>
  <p>
    You can toss some well-appreciated coins at the project through
    <a href="https://www.buymeacoffee.com/jcolag" style="white-space: nowrap;">
      <img
        src="images/bmc-logo.png"
        style="height: 1em; transform: translateY(15%);"
        title="Donate using Buy Me a Coffee"
      >
      Buy Me a Coffee
    </a>
    or
    <a href="https://liberapay.com/jcolag/" style="white-space: nowrap;">
      <img
        src="https://liberapay.com/assets/widgets/donate.svg"
        style="height: 1.25em; transform: translateY(25%);"
        title="Donate using Liberapay"
      >
      LiberaPay
     </a>
  </p>
  <h2>Guidelines</h2>
  <p>
    For any contributions more sophisticated than money, please understand
    that you&rsquo;ll need to agree to two major conditions.
  </p>
  <ul>
    <li>
      Either because the work gets published as a part of
      <b>The Light&rsquo;s Edge</b>, or because it derives from
      <b>The Light&rsquo;s Edge</b>, you must agree to make all contributions
      available under the terms of some public license compatible with
      <a href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>.
      In short, this means that you always retain your copyright, and so can do
      whatever you please with your work in the future, <i>but</i> anybody
      can also use this version for any purpose without your permission,
      provided that they follow the terms of the license in their work.
    </li>
    <li>
      You must agree to abide by the terms of the
      <a href="coc.html">Code of Conduct</a>.
      This community, no matter how small it remains or how large it grows,
      will not stand for abusive behavior, even when disguised as polite
      discourse.
    </li>
  </ul>
  <p>
    If either condition doesn't fit with how you would prefer to operate, then
    the project probably won&rsquo;t suit you, though you can always use its
    results under the terms of the aforementioned license.
  </p>
  <h2>One-Off Artistic Contributions</h2>
  <p>
    <b>The Light&rsquo;s Edge</b> (ideally) spans as many artistic mediums as
    possible, from prose to toys, and more.  If you have the skill and
    interest, please consider introducing yourself at the project&rsquo;s
    <a href="https://jcolag.limesurvey.net/683932?lang=en">Artist RFQ survey</a>
    so that we can keep you in mind and reach out to you for help.
  </p>
  <p>
    That sounds absurdly impersonal, but it ensures that the project has a
    record of your interest and what you do, so that nobody forgets about you.
    The alternative looks suspiciously like somebody frantically asking
    <i>didn&rsquo;t somebody mention enjoying animating things?</i> (or other
    form of art) while digging through social media history.
  </p>
  <h2>Ongoing or Integral Contributions</h2>
  <p>
    <b>The Light&rsquo;s Edge</b> uses
    <a href="https://codeberg.org/">Codeberg</a>
    to manage contributions, including suggestions (or &ldquo;issues&rdquo;)
    and direct alterations to the story.  You'll need an account over there,
    which might sound like effort, but they don't ask for much and have
    <a href="https://docs.codeberg.org/getting-started/first-steps/">
      good documentation on making sense of the site
    </a>
    to read and follow.
  </p>
  <p>
    You&rsquo;ll particularly want go become familiar with
    <a href="https://docs.codeberg.org/getting-started/issue-tracking-basics/">issues</a>,
    and <i>maybe</i> start looking over an
    <a href="https://bcgov.github.io/ds-intro-to-git/materials/01-what-is-vc.html">
      introductory git tutorial
    </a> written for non-programmers, so that you have some confidence to
    make changes without worrying that you might &ldquo;break&rdquo; something.
  </p>
  <p>
    More detail to come, such as where to find this alleged repository, when
    the story actually launches&hellip;
  </p>
  <hr>
  <p>
    <a
      rel="license"
      href="http://creativecommons.org/licenses/by-sa/4.0/">
        <img
          alt="Creative Commons Attribution Share-Alike 4.0 License"
          style="border-width:0; vertical-align:middle;"
          title="Creative Commons Attribution Share-Alike 4.0 License"
          src="images/by-sa.svg"
      />
    </a>
  </p>
  <p style="font-size: 0.75rem;">
    Visit <a href="https://thelightsedge.com"><b>The Light&rsquo;s Edge</b></a>.
  </p>
  <noscript><p><img referrerpolicy="no-referrer-when-downgrade" src="https://colagioia.net/pa/matomo.php?idsite=17&amp;rec=1" style="border:0;" alt="" /></p></noscript>
</body>
</html>
