// SPDX-FileCopyrightText: 2024 John Colagioia <jcolag@colagioia.net>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

<?php
$config = parse_ini_file("config.ini");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $email = $_POST["email"];
  $address = $config['address'];

  if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $to = $address;
    $subject = "New email subscription";
    $message = "Email: $email";

    mail($to, $subject, $message);
    echo "Email submitted successfully!";
  } else {
    echo "Invalid email address!";
  }
} else {
  echo "Method not allowed!";
}
?>
