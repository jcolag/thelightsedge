<!--
SPDX-FileCopyrightText: 2024 John Colagioia <jcolag@colagioia.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# The Light's Edge

The Light's Edge is an ongoing, multimedia space opera story.  This repository will contain material needed to generate the main website at https://thelightsedge.com

## Asset Licenses

This website uses the following media.

 * [*Abandoned nightclub*](https://www.flickr.com/photos/39698489@N00/3563809396) by [Ioan Sameli](https://www.flickr.com/photos/biwook/), made available under the terms of the CC BY-SA 2.0 license.
 * [assembly](https://thenounproject.com/icon/assembly-6638971/) by [Adi Waluyo Noto Carito](https://thenounproject.com/creator/natacarita22/), made available under the terms of the CC BY 3.0 license.
 * [Compass](https://thenounproject.com/icon/compass-5559757/) by [Adi Waluyo Noto Carito](https://thenounproject.com/creator/natacarita22/), made available under the terms of the CC BY 3.0 license.
 * [Eunomia](https://dotcolon.net/font/eunomia/) by Sora Sagano, made available under the terms of the CC0 1.0 license.
 * [*free seamless texture recycled brick, seier+seier*](https://www.flickr.com/photos/seier/4343692655/) by [seier+seier](https://www.flickr.com/photos/seier/), made available under the terms of the CC BY 2.0 license.
 * [Hyper Scrypt](https://velvetyne.fr/fonts/hyper-scrypt/) by [Jérémy Landes](https://velvetyne.fr/authors/jjjlllnnn/), made available under the terms of the SIL OFL 1.1 license.
 * [Inclusive Sans](https://www.oliviaking.com/inclusive-sans) by [Olivia King](https://www.oliviaking.com/about), made available under the terms of the SIL OFL 1.1 license.
 * [League Spartan](https://www.theleagueofmoveabletype.com/league-spartan) by [Caroline Hadilaksono](http://hadilaksono.com)  [Micah Rich](https://micahrich.com)  & [Tyler Finck](http://tylerfinck.com), made available under the terms of the SIL OFL 1.1 license.
 * [legal](https://thenounproject.com/icon/legal-5757691/) by [Adi Waluyo Noto Carito](https://thenounproject.com/creator/natacarita22/), made available under the terms of the CC BY 3.0 license.
 * [Map of the political world in triaxial boreal projection](https://commons.wikimedia.org/wiki/File:Hellerick_triaxial_boreal_projection.svg) by [Hellerick](https://commons.wikimedia.org/wiki/User:Hellerick), made available under the terms of the CC BY-SA 3.0 license.
 * [NeonderThaw](https://github.com/googlefonts/neonderthaw) by Robert Leuschke, made available under the terms of the SIL OFL 1.1 license.
 * [Tilt Neon](https://github.com/googlefonts/Tilt-Fonts) by Andy Clymer, made available under the terms of the SIL OFL 1.1 license.
 * [Vector image of elephant's head sign](https://freesvg.org/vector-image-of-elephants-head-sign) (modified), made available under the terms of the CC0 1.0 license.

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)
